class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxon.roots
    @products = @taxon.all_products.includes(variants: [:images, :prices])
  end
end
