# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe 'GET show' do
    let(:taxonomy) { create(:taxonomy, name: "Brand") }
    let(:taxon) { create(:taxon, name: "Bag", products: [product]) }
    let!(:product) { create(:product) }
    let!(:image) { build(:image) }

    before do
      product.images << image
      get potepan_category_path(taxon.id)
    end

    it 'レスポンスが成功していること' do
      expect(response).to be_successful
    end

    it 'get request' do
      expect(response.status).to eq 200
    end

    it 'instanceが有効であること' do
      expect(taxon).to be_valid
    end

    it 'showテンプレートをレンダリングしていること' do
      expect(response).to render_template :show
    end

    it 'bodyタグの中にtaxon.nameとtaxonomy.nameが表示されていること' do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include taxon.name
    end

    it "カテゴリーに属する商品が表示されていること" do
      expect(response.body).to include product.name
    end
  end
end
