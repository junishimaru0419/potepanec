# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :request do
  describe 'GET show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product) }
    let!(:related_product) { create_list(:product, 5, taxons: [taxon]) }
    let!(:image) { build(:image) }

    before do
      product.images << image
      get potepan_product_path(product.id)
    end

    it 'レスポンスが成功していること' do
      expect(response).to be_successful
    end

    it 'get request' do
      expect(response.status).to eq 200
    end

    it 'instance が有効であるこt' do
      expect(product).to be_valid
    end

    it 'showテンプレートをレンダリングしていること' do
      expect(response).to render_template :show
    end

    it 'bodyタグの中にproduct.nameとproduct.descriptionが表示されること' do
      expect(response.body).to include product.name
      expect(response.body).to include product.description
    end

    it '5件以上データはあるが関連商品が4つ表示されていること' do
      expect(assigns(:related_products).size).to eq 4
    end
  end
end
