# frozen_string_literal: true

RSpec.describe Spree::Product, type: :model do
  describe "get ralated_products" do
    let(:taxon_1) { create(:taxon) }
    let(:taxon_2) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon_1]) }
    let!(:related_product_taxon_1) { create(:product, taxons: [taxon_1]) }
    let!(:other_product_taxon_2) { create(:product, taxons: [taxon_2]) }

    it "related_productが含まれていること" do
      expect(product.related_products).to match_array related_product_taxon_1
    end

    it "other_productが含まれていないこと" do
      expect(product.related_products).not_to match_array other_product_taxon_2
    end
  end
end
