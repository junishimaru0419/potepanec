# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :helper do
  describe 'GET #show' do
    include ApplicationHelper

    it 'page_title blank test' do
      expect(full_title(page_title: '')).to eq 'BIGBAG Store'
    end

    it 'page_title no blank test' do
      expect(full_title(page_title: 'Ruby on Rails Tote')).to eq 'Ruby on Rails Tote - BIGBAG Store'
    end

    it 'page_title nil test' do
      expect(full_title(page_title: nil)).to eq 'BIGBAG Store'
    end
  end
end
