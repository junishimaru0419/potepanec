# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Potepan::Products", type: :system do
  describe 'GET show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let!(:image) { build(:image) }

    before do
      product.images << image
      visit potepan_product_path(product.id)
    end

    it '「一覧ページへ戻る」をクリックすると、商品一覧ページに戻ること' do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(related_product.taxons.first.id)
    end

    it 'Homeボタンクリックでトップページに移動すること' do
      visit potepan_path
      click_link 'Home'
      expect(page).to have_content("BIGBAG")
    end

    it '関連商品のnameとdisplay_priceが表示されていること' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end

    it "関連商品の商品名をクリックすると、商品詳細ページに移動する" do
      click_link related_product.name
      expect(current_path).to eq potepan_product_path(related_product.id)
    end
  end
end
