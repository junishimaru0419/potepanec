# frozen_string_literal: true

require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  describe 'GET show' do
    let(:taxonomy) { create(:taxonomy, name: "Brand") }
    let(:taxon) { create(:taxon, name: "Bag", taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:image) { build(:image) }

    before do
      product.images << image
      visit potepan_category_path(taxon.id)
    end

    it 'カテゴリー名をクリックすると分類と商品数が表示されること' do
      within '.side-nav' do
        click_link taxonomy.name
        expect(page).to have_content taxon.all_products.size
      end
    end

    it 'クリックしたリンクが正常であること' do
      click_link taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    it '商品が正常に表示されていること' do
      within '.productsContent' do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
      end
    end

    it 'Homeボタンクリックでトップページに移動すること' do
      visit potepan_path
      click_link 'Home'
      expect(page).to have_content("BIGBAG")
    end
  end
end
